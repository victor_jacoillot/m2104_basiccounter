﻿Imports BasicCounterClass

Public Class Form1

    Dim CounterClass As New CounterClass

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CounterClass.btn_decrement()
        Counter.Text = CounterClass.getNombre_Compteur()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        CounterClass.btn_increment()
        Counter.Text = CounterClass.getNombre_Compteur()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        CounterClass.btn_reset()
        Counter.Text = CounterClass.getNombre_Compteur()
    End Sub
End Class
