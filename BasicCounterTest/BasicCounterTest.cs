﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterClass;

namespace BasicCounterTest
{
    [TestClass]
    public class BasicCounterTest
    {
        [TestMethod]
        public void TestCompteurConstructeur()
        {
            CounterClass counter = new CounterClass();
            Assert.AreEqual(0, counter.getNombre_Compteur());
            
        }

        [TestMethod]
        public void TestCompteurDecre()
        {
            CounterClass counter = new CounterClass();
            counter.btn_decrement();
            Assert.AreEqual(-1, counter.getNombre_Compteur());

        }
        [TestMethod]
        public void TestCompteurIncre()
        {
            CounterClass counter = new CounterClass();
            counter.btn_increment();
            Assert.AreEqual(1, counter.getNombre_Compteur());

        }
        [TestMethod]
        public void TestCompteurRAZ()
        {
            CounterClass counter = new CounterClass();
            counter.btn_reset();
            Assert.AreEqual(0, counter.getNombre_Compteur());

        }
    }
}
