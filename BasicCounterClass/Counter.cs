﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterClass
{
    public class CounterClass
    {
        private int nombre_compteur;

        public CounterClass()
        {
            this.nombre_compteur = 0;
        }

        public int btn_decrement()
        {
           return nombre_compteur--;
        }
        public int btn_increment()
        {
            return nombre_compteur++;
        }

        public int btn_reset()
        {
           return nombre_compteur = 0;
        }

        public int getNombre_Compteur()
        {
            return this.nombre_compteur;
        }
    }
}
